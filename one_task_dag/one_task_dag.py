#!/usr/bin/python3
# -*- coding: utf-8 -*-

from datetime import timedelta
from airflow import DAG
from airflow.operators.bash_operator import BashOperator
from airflow.utils.dates import days_ago


default_args = {
    'owner': 'airflow',
    'start_date': days_ago(0, minute=5),
    'retries': 1,
    'retry_delay': timedelta(seconds=5)
}

dag = DAG(dag_id='one_task_dag',
          default_args=default_args,
          schedule_interval='0 */1 * * *',
          max_active_runs=1)


hello_boid_func =  BashOperator(
    task_id='print_date',
    bash_command='date',
    dag=dag,
)
